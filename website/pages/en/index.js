const React = require("react");

/**
 * Docente
 */
const docenteLinks = {
  columns: [
    {
      rows: [
        {
          title: "Gerais",
          perguntas: [
            { title: "Que informações compõem o dashboard do professor?", link: "dashboard-do-professor" },
            { title: "Como recuperar a minha senha?", link: "recuperar-senha" },
            { title: "Como acessar a lixeira?", link: "acessar-lixeira" },
            { title: "Como gerenciar minhas notificações?", link: "notificacao" }
          ]
        },
        {
          title: "Questões",
          perguntas: [
            { title: "Como funciona o compartilhamento de questões?", link: "questao-associacao-coluna" },
            { title: "Como funciona a publicação de questões?", link: "questao-associacao-coluna" },
            { title: "Como elaborar uma questão de associação de colunas?", link: "questao-associacao-coluna" },
            { title: "Como elaborar uma questão V ou F?", link: "questao-vouf" },
            { title: "Como elaborar uma questão objetiva?", link: "questao-objetiva" },
            { title: "Como elaborar uma questão discursiva?", link: "questao-discursiva" },
            { title: "Como elaborar uma questão do tipo bloco?", link: "questao-bloco" },
            { title: "Que informações são exibidas no card da questão?", link: "questao-bloco" },
            { title: "Como associar tags às questões?", link: "questao-bloco" },
            { title: "Como fazer a filtragem avançada de questões?", link: "questao-bloco" }
          ]
        },
        {
          title: "Turmas",
          perguntas: [
            { title: 'Como cadastrar uma turma?', link: 'turma-criar' },
            { title: 'Como inserir um aluno em uma turma?', link: 'turma-criar' },
            { title: 'Como inserir alunos em lote em uma turma?', link: '' },
          ]
        },
      ]
    },
    {
      rows: [
        {
          title: "Provas",
          perguntas: [
            { title: 'Como funciona a correção das questões objetivas?', link: '' },
            { title: 'Como atribuir notas e pesos a questões de uma prova?', link: '' },
            { title: 'Como corrigir as questões de uma prova?', link: '' },
            { title: 'Que informações compõem o painel da prova?', link: '' },
            { title: 'Quais as informações estatísticas disponíveis para uma prova?', link: '' },
            { title: 'Quais os status que um prova pode ter?', link: '' },
            { title: 'Como gerenciar a vista de prova?', link: '' },
            { title: 'Como preparar uma prova convencional?', link: 'prova-embaralhamento-geracao-provas' },
            { title: 'Como preparar uma prova dinâmica?', link: 'prova-participantes' },
            { title: 'Como associar participantes a uma prova?', link: 'prova-sorteio-questoes' },
            { title: 'Como aplicar uma prova virtual?', link: 'prova-aplicar-virtual' },
            { title: 'Que tipo de embaralhamento é feito na geração das provas?', link: 'prova-convencional' },
            { title: 'Como publicar as notas de uma prova para meus alunos?', link: '' },
          ]
        },
        {
          title: "Editor",
          perguntas: [
            { title: 'Que recursos de formatação de texto estão disponíveis no editor?', link: '' },
            { title: 'Que recursos de formatação de parágrafo estão disponíveis no editor?', link: '' },
            { title: 'Como inserir fórmulas matemáticas com o editor?', link: '' },
            { title: 'Como inserir imagens com o editor?', link: '' },
            { title: 'Como inserir e formatar linhas horizontais com o editor?', link: '' },
            { title: 'Como inserir e formatar caixas de texto com o editor?', link: '' },
            { title: 'Como inserir e formatar tabelas com o editor?', link: '' },
            { title: 'Como inserir parágrafos antes e depois de elementos no editor?', link: '' },
            { title: 'Como inserir símbolos especiais com o editor?', link: '' },
          ]
        }
      ]
    }
  ]
};

/**
 * Discente
 */

const discenteLinks = {
  columns: [
    {
      rows: [
        {
          title: "Geral",
          perguntas: [
            { title: 'Que informações compõem o dashboard?', link: 'dashboard-do-professor' },
            { title: 'Como recuperar a minha senha?', link: 'recuperar-senha' },
          ]
        },
        {
          title: "Provas",
          perguntas: [
            { title: 'Como funciona a correção das questões objetivas?', link: '' },
            { title: 'Quais as informações estatísticas disponíveis para uma prova?', link: '' },
            { title: 'Como fazer a vista de prova?', link: '' },
            { title: 'Como responder questões objetivas?', link: '' },
            { title: 'Como responder questões tipo V ou F?', link: '' },
            { title: 'Como responder questões de associação de colunas?', link: '' },
            { title: 'Como responder questões discursivas?', link: '' },
            { title: 'Como navegar pela prova?', link: '' },
            { title: 'Que tipo de informação está disponível na vista de prova?', link: '' },
            { title: 'Por que a vista de prova ou a minha nota não está disponível para mim?', link: '' },
          ]
        },
      ]
    },
    {
      rows: [
        {
          title: "Editor",
          perguntas: [
            { title: 'Que recursos de formatação de texto estão disponíveis no editor?', link: '' },
            { title: 'Que recursos de formatação de parágrafo estão disponíveis no editor?', link: '' },
            { title: 'Como inserir fórmulas matemáticas com o editor?', link: '' },
            { title: 'Como inserir imagens com o editor?', link: '' },
            { title: 'Como inserir e formatar linhas horizontais com o editor?', link: '' },
            { title: 'Como inserir e formatar caixas de texto com o editor?', link: '' },
            { title: 'Como inserir e formatar tabelas com o editor?', link: '' },
            { title: 'Como inserir parágrafos antes e depois de elementos no editor?', link: '' },
            { title: 'Como inserir símbolos especiais com o editor?', link: '' },
          ]
        }
      ]
    }
  ]
};

/**
 * Gestor
 */

const gestorLinks = {
  columns: [
    {
      rows: [
        {
          title: "Geral",
          perguntas: [
            { title: 'Que informações compõem o dashboard?', link: 'dashboard-do-professor' },
            { title: 'Como recuperar a minha senha?', link: 'recuperar-senha' },
          ]
        },
      ]
    },
    {
      rows: []
    }
  ]
};

/**
 * Admin
 */
const adminLinks = {
  columns: [
    {
      rows: [
        {
          title: "Geral",
          perguntas: [
            { title: 'Como gerenciar os usuários cadastrados no sistema?', link: '' },
            { title: 'Como cadastrar alunos em lote no sistema?', link: '' },
            { title: 'Como fazer a configuração do sistema?', link: '' },
          ]
        },
      ]
    },
    {
      rows: []
    }
  ]
};

class Index extends React.Component {

  get data() {
    const url = new URL(window.location.href)
    const target = url.searchParams.get("target")

    if (target !== null || target !== undefined) {

    }
  }

  render() {
    const { config: siteConfig, } = this.props;

    const Duvidas = () => {
      return (
        <div>
          <div id="progress">
            <div className="grid container-home center">
              <div class="lds-facebook"><div></div><div></div><div></div></div>
            </div>
            <div className="grid container-home center">Carregando links</div>
          </div>

          <div className="grid container-home" id="conteudo"></div>

          <div className="grid container-home" id="docente">
            {docenteLinks.columns.map((column, c) => (
              <div key={c} className="col-6 p-2">
                {column.rows.map((row, r) => (
                  <div key={r}>
                    <h2>{row.title}</h2>
                    <ul>
                      {row.perguntas.map((pergunta, p) => (
                        <li>
                          <a key={p} href={`docs/${pergunta.link}`}>
                            {pergunta.title}
                          </a>
                        </li>
                      ))}
                    </ul>
                  </div>
                ))}
              </div>
            ))}
          </div>

          <div className="grid container-home" id="discente">
            {discenteLinks.columns.map((column, c) => (
              <div key={c} className="col-6 p-2">
                {column.rows.map((row, r) => (
                  <div key={r}>
                    <h2>{row.title}</h2>
                    <ul>
                      {row.perguntas.map((pergunta, p) => (
                        <li>
                          <a key={p} href={`docs/${pergunta.link}`}>
                            {pergunta.title}
                          </a>
                        </li>
                      ))}
                    </ul>
                  </div>
                ))}
              </div>
            ))}
          </div>

          <div className="grid container-home" id="gestor">
            {gestorLinks.columns.map((column, c) => (
              <div key={c} className="col-6 p-2">
                {column.rows.map((row, r) => (
                  <div key={r}>
                    <h2>{row.title}</h2>
                    <ul>
                      {row.perguntas.map((pergunta, p) => (
                        <li>
                          <a key={p} href={`docs/${pergunta.link}`}>
                            {pergunta.title}
                          </a>
                        </li>
                      ))}
                    </ul>
                  </div>
                ))}
              </div>
            ))}
          </div>

          <div className="grid container" id="admin">
            {adminLinks.columns.map((column, c) => (
              <div key={c} className="col-6 p-2">
                {column.rows.map((row, r) => (
                  <div key={r}>
                    <h2>{row.title}</h2>
                    <ul>
                      {row.perguntas.map((pergunta, p) => (
                        <li>
                          <a key={p} href={`docs/${pergunta.link}`}>
                            {pergunta.title}
                          </a>
                        </li>
                      ))}
                    </ul>
                  </div>
                ))}
              </div>
            ))}
          </div>
        </div>
      );
    };

    return (
      <div>
        <div>
          <Duvidas />
        </div>
      </div>
    );
  }
}

module.exports = Index;
